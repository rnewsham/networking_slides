#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';

my %data = (
			foo => 2,
			bar => 1,
			foobar => 3,
			);

my $max = ( sort { $data{$b} <=> $data{$a} } keys %data)[0];
say $max;


