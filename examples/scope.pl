#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';


my $x = 1;

say $x; # 1

{
	my $x = 2;
	say $x; # 2 
}

say $x; # 1
