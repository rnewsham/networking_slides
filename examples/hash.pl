#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Data::Dumper;

my %foo = (
			bar => 'bob',
			car => 'cob',
			dar => 'dob',
			);

say $foo{bar}; #bob
say join( ', ', keys %foo ); #bar, car, dar

delete $foo{car};
say join( ', ', keys %foo ); #bar, dar

