#!/usr/bin/env perl
#
use strict;
use warnings;

use feature 'say';

my $foo = "11.1234";
#Straightforward traditional print
print "$foo\n";
#Modern way automatically suffixes a newline:
say $foo;
#Formatting numbers
printf( "%.2f\n", $foo );
#print to filehandle
print STDERR "$foo\n";

