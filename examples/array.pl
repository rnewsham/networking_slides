#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use feature 'say';

my @drinks = ( 1, 2, 3 );
my @bar = qw( beer wine cider );

say shift @bar; #beer
say pop @bar; #cider

push @bar, 'whiskey';

say join(', ', @bar); # wine, whiskey
